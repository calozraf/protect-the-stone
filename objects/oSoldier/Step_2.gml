/// @description Follow path planner

motion_add(point_direction(x,y, planner.x, planner.y),0.1);

if (speed > 1)
{
	speed = 1;	
}

target_x = oHideout.bbox_left-GRIDSIZE;
target_y = oHideout.bbox_top-GRIDSIZE;

with (planner)
{
	x = follower.x;
	y = follower.y;
	plan(follower.target_x, follower.target_y);
}