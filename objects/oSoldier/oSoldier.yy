{
    "id": "e3dbb0c9-769a-481b-a39a-59536dffc087",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSoldier",
    "eventList": [
        {
            "id": "83038f4f-d633-440b-9e44-3b13d074ef76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e3dbb0c9-769a-481b-a39a-59536dffc087"
        },
        {
            "id": "9b6980ec-a046-4cb0-95cb-72bfdd56e7bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "e3dbb0c9-769a-481b-a39a-59536dffc087"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "af49a9ab-db0b-415d-93a1-ea26b23fc460",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "30c0ed75-5dad-4628-9280-aadca13e6c67",
    "visible": true
}