{
    "id": "d56d23cb-9dfd-4fb0-98a3-5f703704e679",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSelector",
    "eventList": [
        {
            "id": "a4d9e4cf-78bd-4d62-bc15-ec33865e5e7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "d56d23cb-9dfd-4fb0-98a3-5f703704e679"
        },
        {
            "id": "79bdbb18-7ce4-4535-bfcd-840bb765911a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d56d23cb-9dfd-4fb0-98a3-5f703704e679"
        },
        {
            "id": "83b6ce3f-ead4-400a-a302-3040480b1226",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d56d23cb-9dfd-4fb0-98a3-5f703704e679"
        },
        {
            "id": "9ee34d7b-377c-44a1-a842-6a25f17a1115",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d56d23cb-9dfd-4fb0-98a3-5f703704e679"
        },
        {
            "id": "d907fb19-52f6-4e1d-963a-98905a7a61d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "d56d23cb-9dfd-4fb0-98a3-5f703704e679"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dbcf53ce-0870-4507-8111-35644ed909df",
    "visible": true
}