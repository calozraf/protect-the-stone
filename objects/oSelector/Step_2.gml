/// @description Ins
x = mouse_x;
y = mouse_y;

x = clamp(x,global.vx,VIEW_WIDTH+global.vx-sprite_width);
y = clamp(y,global.vy,VIEW_HEIGHT+global.vy-sprite_height)

if (mouse_check_button_pressed(mb_left))
{
	with(oChimera)
	{
		selected = false;	
	}
}

if (global.buildmode)
{
	if (buildingSprite != -1)
	{
		sx = mouse_x - mouse_x%GRIDSIZE;
		sy = mouse_y - mouse_y%GRIDSIZE;
		
		/*
		show_debug_message("__________________________")
		show_debug_message(sx)
		show_debug_message(sy)
		*/
		
		if (mouse_check_button_pressed(mb_left))
		{
			var inst = instance_position(sx,sy,oFreeTile);
			if (inst != noone  && !place_meeting(x,y,oEnemy))
			{
				instance_destroy(inst);
				var newB = instance_create_layer(sx,sy,"Instances",spr_to_obj(buildingSprite));
				global.placingObj = true;
				al0Exe = false;
				alarm[0] = 3;//To reset placing obj game state and allow opening another building's menu
				if (newB.object_index != oFarm)
				{
					mp_grid_add_cell(grid,sx/GRIDSIZE,sy/GRIDSIZE);
				}
			}
			else
			{
				floating_text(x,y,"Can't build here!",c_red);
			}
		}
	}
}
else
{
	buildingSprite = -1;
	if (mouse_check_button_pressed(mb_right))
	{
		alarm[1] = SWIPE_TIME;
		previous_mouse_x = mouse_x;
		previous_mouse_y = mouse_y;
	}
	if (mouse_check_button_released(mb_right) && recting)
	{
		recting = false;
		select_units_within_rectangle();
	}
}