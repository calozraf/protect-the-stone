{
    "id": "2e336fb0-c0a4-4f60-8dad-0b2d90c014db",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCopper",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4cf6f271-109e-40d8-9337-15d7a8870aef",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dd7cb858-d092-4d5c-8f4b-c2af9dd70579",
    "visible": true
}