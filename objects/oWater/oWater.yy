{
    "id": "5c4d1cad-fccf-466f-aa19-756bbb4b8557",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWater",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4cf6f271-109e-40d8-9337-15d7a8870aef",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "36c417b2-abe7-42d0-befe-321f7bd4ee8d",
    "visible": true
}