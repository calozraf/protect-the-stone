/// @description Ins

x = ogX + global.vx;
y = ogY + global.vy;

if (mouse_over_ui())
{
	image_blend = c_yellow;
	if (mouse_check_button_pressed(mb_left))
	{
		with (oButton) { selected = false; }
		selected = true;
		set_desc(object_index);
	}
}
else
{
	image_blend = c_white;	
}

if (selected)
{
	image_alpha = 1;
	if (mouse_y-global.vy < VIEW_HEIGHT*0.667)
	{
		oSelector.buildingSprite = sprite_index;
	}
	else
	{
		oSelector.buildingSprite = -1;
	}
}
else
{
	image_alpha = 0.65;	
}

if (!global.buildmode) instance_destroy();