{
    "id": "4fbf3f72-e909-49f6-9848-6478d92848c4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oUIBox",
    "eventList": [
        {
            "id": "96e5df0b-0e65-4d4d-a959-87331e751b4a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4fbf3f72-e909-49f6-9848-6478d92848c4"
        },
        {
            "id": "2f151632-539b-4335-877b-428207fabecc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4fbf3f72-e909-49f6-9848-6478d92848c4"
        },
        {
            "id": "a55368b4-e634-42a0-87f4-0467bbe12e77",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4fbf3f72-e909-49f6-9848-6478d92848c4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ca6f9f5d-b9b1-4d60-81e8-03bf370de1a6",
    "visible": true
}