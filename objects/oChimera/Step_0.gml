/// @description Ins
if (hp <= 0)
{
	instance_destroy();	
}

if (selected)
{
	if(global.flagIsActive)
	{
		xTo = global.flagX;
		yTo = global.flagY;
	
		//motion_add(point_direction(x,y,xTo,yTo),maxSpd*0.1);
	
		if (spd < maxSpd)
		{
			spd += maxSpd*0.05;
		}
	}
}

//Move
var dir = point_direction(x,y,xTo,yTo);
	
xSpd = lengthdir_x(spd,dir);
ySpd = lengthdir_y(spd,dir);

var inst = instance_place(x,y,oChimera);
if (inst != noone)
{
	xSpd += (x-inst.x)*0.1;
	ySpd += (y-inst.y)*0.1;
}
	
	
x += xSpd;
y += ySpd;

if (point_distance(x,y,xTo,yTo)<2)
{
	spd = 0;
	if (xTo == global.flagX && yTo == global.flagY)
	{
		global.flagIsActive = false;	
	}
}