{
    "id": "b5596770-c620-41bf-9bb3-329a450a6aa9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oOwlWasp",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1df16e3c-a629-4761-8fa3-274bf1178d04",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "217e36cd-be5c-4195-99ab-384f4b21880a",
    "visible": true
}