{
    "id": "65be1d49-7a5c-4b4f-a11c-a11a1b95104c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oQuarryButton",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d3daa07c-fde9-48e6-b416-9d7839a43f7d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "69d1613e-2b35-4115-bdaa-4c662c2831dc",
    "visible": true
}