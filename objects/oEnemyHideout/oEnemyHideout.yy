{
    "id": "1670db37-1add-4a93-961a-1853ba14ae8e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemyHideout",
    "eventList": [
        {
            "id": "18a9e381-3a4b-4d54-9a4d-10396b553bd0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1670db37-1add-4a93-961a-1853ba14ae8e"
        },
        {
            "id": "1526c26a-f83e-46dc-a381-746518abd0f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1670db37-1add-4a93-961a-1853ba14ae8e"
        },
        {
            "id": "4e87f68e-716d-486e-82b6-6a829e772b2c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1670db37-1add-4a93-961a-1853ba14ae8e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4cf6f271-109e-40d8-9337-15d7a8870aef",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1ef4feed-6a42-4ff6-b1ea-6be2bd453af5",
    "visible": true
}