/// @description Ins
var obstacle = instance_place(x,y+GRIDSIZE,oObstacle);
if (obstacle != noone)
{
	//instance_create_layer(x,y+GRIDSIZE,"Instances",oSoldier);
	mp_grid_clear_cell(grid,obstacle.x/GRIDSIZE,obstacle.y%GRIDSIZE)
	instance_destroy(obstacle);
}
	
var freeTile = instance_position(x,y+GRIDSIZE,oFreeTile);
if (freeTile != noone)
{
	instance_destroy(freeTile);
}