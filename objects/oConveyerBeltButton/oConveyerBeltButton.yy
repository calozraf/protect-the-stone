{
    "id": "c5485081-0b96-4b8b-ab48-1dffb8ed96f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oConveyerBeltButton",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d3daa07c-fde9-48e6-b416-9d7839a43f7d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e876745a-81c6-427d-98f2-22b5b4235805",
    "visible": true
}