/// @description Ins
var pad = 8;

var key_left = kc(vk_left) || kc(ord("A")) || mouse_x < global.vx + pad;
var key_right = kc(vk_right) || kc(ord("D")) || mouse_x > global.vx + VIEW_WIDTH - pad;
var key_up = kc(vk_up) || kc(ord("W")) || mouse_y < global.vy + pad;
var key_down = kc(vk_down) || kc(ord("S")) || mouse_y > global.vy + VIEW_HEIGHT - pad;

var spd = 7;

if (key_left) x -= spd;
if (key_right) x += spd;
if (key_down) y += spd;
if (key_up) y -= spd;

x = clamp(x,VIEW_WIDTH/2,room_width-VIEW_WIDTH/2);
y = clamp(y,VIEW_HEIGHT/2,room_height-VIEW_HEIGHT/2);