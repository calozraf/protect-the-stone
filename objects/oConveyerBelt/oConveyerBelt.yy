{
    "id": "d8e532db-154d-46c4-9d06-3fab9752ebc5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oConveyerBelt",
    "eventList": [
        {
            "id": "07e8c76a-5b4b-4d2a-92c6-52971dd195f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d8e532db-154d-46c4-9d06-3fab9752ebc5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b71ac85a-bd6d-4532-9535-444ad8441049",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e876745a-81c6-427d-98f2-22b5b4235805",
    "visible": true
}