{
    "id": "f5789e3b-4abd-43ab-8b12-d991c4cc8a31",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTree",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4cf6f271-109e-40d8-9337-15d7a8870aef",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "773a6b3c-62f6-4544-9cb1-39e72d293b4e",
    "visible": true
}