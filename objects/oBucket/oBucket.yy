{
    "id": "e6d958ae-442f-4d33-8a03-dc8afdf0563f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBucket",
    "eventList": [
        {
            "id": "dc38e8ea-a4e9-49f2-87c4-62e952ede28b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e6d958ae-442f-4d33-8a03-dc8afdf0563f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "328915da-8a71-483d-93e1-c3c48cacdb23",
    "visible": true
}