/// @description Ins
x = ogX + global.vx;
y = ogY + global.vy;

if (mouse_over_ui())
{
	image_blend = c_yellow;
	if (mouse_check_button_pressed(mb_left))
	{
		instance_create_layer(oUIBox.buildingId.x,oUIBox.buildingId.y,"Instances",oFreeTile);
		instance_destroy(oUIBox.buildingId);
		global.buildmode = false;
	}
}

if (!global.buildmode) instance_destroy();