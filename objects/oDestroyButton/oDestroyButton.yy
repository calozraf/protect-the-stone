{
    "id": "f9e922ae-013e-42a7-9537-e6218d819012",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDestroyButton",
    "eventList": [
        {
            "id": "69962d8d-7d18-449e-83a3-1924a8f1e8d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f9e922ae-013e-42a7-9537-e6218d819012"
        },
        {
            "id": "c471d2ff-7632-4800-bcc7-08ecf2e71138",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f9e922ae-013e-42a7-9537-e6218d819012"
        },
        {
            "id": "ba8c2f3f-0f54-4f45-882b-e9bf1c5fe56b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f9e922ae-013e-42a7-9537-e6218d819012"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "279d6d50-1399-42fd-9fda-deda14ddb4d6",
    "visible": true
}