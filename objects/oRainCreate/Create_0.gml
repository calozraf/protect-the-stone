/// @description Init system

c_light_green = make_color_rgb(192, 237, 199);

//Rain System
partRain_sys = part_system_create();
part_system_depth(partRain_sys,depth);

//Rain Particle
partRain = part_type_create();
part_type_shape(partRain,pt_shape_square);
part_type_size(partRain,0.04*0.3,0.08*0.3,0,0);
part_type_color1(partRain,c_green);
part_type_alpha2(partRain,.5,.1);
//part_type_gravity(partRain,0.1,290);
part_type_speed(partRain,0.4,0.8,0,0.2);
part_type_direction(partRain,260,280,0,0);
part_type_life(partRain,60*20,60*20);

//Rain Particle 2
partRain2 = part_type_create();
part_type_shape(partRain2,pt_shape_square);
part_type_size(partRain2,0.04*0.3,0.08*0.3,0,0);
part_type_color1(partRain2,c_light_green);
part_type_alpha2(partRain2,.5,.1);
//part_type_gravity(partRain,0.1,290);
part_type_speed(partRain2,0.4,0.8,0,0.2);
part_type_direction(partRain2,260,280,0,0);
part_type_life(partRain2,60*20,60*20);

//Create Emitter
partRain_emit = part_emitter_create(partRain_sys);
/// @description Ins
part_emitter_region(partRain_sys,partRain_emit,global.vx,global.vx+VIEW_WIDTH,global.vy,global.vy+VIEW_HEIGHT,ps_shape_rectangle,ps_distr_linear);
part_emitter_burst(partRain_sys,partRain_emit,partRain,1);