{
    "id": "c868ea4c-fe8d-454a-8b6e-b842676b9252",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEarthTemple",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b71ac85a-bd6d-4532-9535-444ad8441049",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b7aac2ff-a262-4477-b960-0e12044ec10e",
    "visible": true
}