{
    "id": "5ad470c6-3960-4ded-a2de-63cff75d6779",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFreeTile",
    "eventList": [
        {
            "id": "2aaf43da-c472-4c6a-9941-833dc194fa55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5ad470c6-3960-4ded-a2de-63cff75d6779"
        },
        {
            "id": "bfa56b52-ddf1-4aaf-83d6-e9a9bb139dcb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5ad470c6-3960-4ded-a2de-63cff75d6779"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7eb320ad-c1e0-432a-ad15-84c089459699",
    "visible": true
}