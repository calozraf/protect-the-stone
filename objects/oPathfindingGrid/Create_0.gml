/// @description Create A* Grid

globalvar grid;

grid = mp_grid_create(0,0, room_width/GRIDSIZE, room_height/GRIDSIZE, GRIDSIZE, GRIDSIZE);