{
    "id": "cbf323a9-d341-43fb-99f5-2a55d2497c15",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPathPlanner",
    "eventList": [
        {
            "id": "9a95b92a-1594-426c-95f4-7964f46ae83d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cbf323a9-d341-43fb-99f5-2a55d2497c15"
        },
        {
            "id": "ba149ef7-056f-4c6b-9308-bed06d01874e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "cbf323a9-d341-43fb-99f5-2a55d2497c15"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bb965b49-b1ac-42a0-afab-c35708618d78",
    "visible": true
}