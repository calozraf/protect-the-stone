{
    "id": "24b95357-d1af-4f67-91d5-2a2a7aba4f5d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oQuarry",
    "eventList": [
        {
            "id": "9338c30f-d6c8-4cad-977c-f8590d3561a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "24b95357-d1af-4f67-91d5-2a2a7aba4f5d"
        },
        {
            "id": "2394fe6b-274b-4d3f-ab10-ce203f490e2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "24b95357-d1af-4f67-91d5-2a2a7aba4f5d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b71ac85a-bd6d-4532-9535-444ad8441049",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "69d1613e-2b35-4115-bdaa-4c662c2831dc",
    "visible": true
}