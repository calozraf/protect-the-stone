/// @description Ins
event_inherited();

left = instance_position(x-GRIDSIZE,y,oObstacle);
right = instance_position(x+GRIDSIZE,y,oObstacle);
top = instance_position(x,y-GRIDSIZE,oObstacle);
bottom = instance_position(x,y+GRIDSIZE,oObstacle);

extractionPoint = exdir.none;
conveyerInst = noone;

if (left != noone)
{
	var lo = left.object_index;
	show_debug_message("Left: " + object_get_name(lo));
	if (lo == oTin || lo == oCopper || lo == oSilver || lo == oGold)
	{
		extractionPoint = exdir.left;
	}
	switch (lo)
	{		
		default: break;
	}
}
if (right != noone)
{
	var ro = right.object_index;
	show_debug_message("Right: " + object_get_name(ro));
	if (ro == oTin || ro == oCopper || ro == oSilver || ro == oGold)
	{
		extractionPoint = exdir.right;
	}
}
if (top != noone)
{
	var to = top.object_index;
	show_debug_message("Up: " + object_get_name(to));
	if (to == oTin || to == oCopper || to == oSilver || to == oGold)
	{
		extractionPoint = exdir.top;
	}
}
if (bottom != noone)
{
	var bo = bottom.object_index;
	show_debug_message("Down: " + object_get_name(bo));
	if (bo == oTin || bo == oCopper || bo == oSilver || bo == oGold)
	{
		extractionPoint = exdir.bottom;
	}
}

if (extractionPoint != noone)
{
	if (excavationTimer <= 0)
	{
		switch (extractionPoint)
		{
			case exdir.bottom:
				create_bucket(bottom);
				break;
			case exdir.left:
				create_bucket(bottom);
				break;
			case exdir.right:
				create_bucket(right);
				break;
			case exdir.top:
				create_bucket(top);
				break;
		}
	}
	else
	{
		excavationTimer--;	
	}
}