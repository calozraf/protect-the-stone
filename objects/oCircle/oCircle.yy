{
    "id": "6eb9ecfd-2ce2-4248-91a9-3f690a0472fb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCircle",
    "eventList": [
        {
            "id": "61d70acd-f40f-4afa-8868-23afc99213ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6eb9ecfd-2ce2-4248-91a9-3f690a0472fb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "80834387-625a-457f-b80b-cea86982fe57",
    "visible": true
}