/// @description Ins
draw_self();

draw_set_font(fnt_consolas);
draw_text(x+8,y+2,itemName);

draw_set_font(fnt_consolas_small);
draw_text_ext(x+8,y+24,itemDesc,12,sprite_width-8*2);

draw_sprite(sWoodRessource,0,x+16,y+sprite_height-16);
draw_sprite(sStoneRessource,0,x+16+32,y+sprite_height-16);
draw_sprite(sFoodRessource,0,x+16+32*2,y+sprite_height-16);
draw_sprite(sWaterRessource,0,x+16+32*3,y+sprite_height-16);