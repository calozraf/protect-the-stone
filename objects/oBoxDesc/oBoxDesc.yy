{
    "id": "2960a149-4d99-45fa-8899-a415fe099a3c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBoxDesc",
    "eventList": [
        {
            "id": "7fee5d65-ff54-4d57-8fbc-9c5627197283",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2960a149-4d99-45fa-8899-a415fe099a3c"
        },
        {
            "id": "b0ec96f3-e684-402b-aafa-5014fe0d3d7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2960a149-4d99-45fa-8899-a415fe099a3c"
        },
        {
            "id": "416f49f2-ac00-462e-b4b5-6214e2a7371d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2960a149-4d99-45fa-8899-a415fe099a3c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4ab1afb2-4de3-4012-93fb-8d502a2af1af",
    "visible": true
}