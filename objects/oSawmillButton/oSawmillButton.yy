{
    "id": "1b66fcd3-fa8a-4fe6-a4b6-8f49e5b4c057",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSawmillButton",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d3daa07c-fde9-48e6-b416-9d7839a43f7d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6c14f54d-b92c-4079-862f-2b6050a749f6",
    "visible": true
}