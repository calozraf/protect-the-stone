/// @description Create lake
instance_create_check(x+GRIDSIZE,y+GRIDSIZE,oWater);
instance_create_check(x,y,oWater);
instance_create_check(x,y+GRIDSIZE,oWater);
instance_create_check(x+GRIDSIZE,y,oWater);


var dir = choose(2,4);
repeat(5)
{
	switch (dir)
	{
		case 1 : x += GRIDSIZE;break;
		case 2 : x -= GRIDSIZE;break;
		case 3 : y += GRIDSIZE;break;
		case 4 : y -= GRIDSIZE;break;
	}
	instance_create_check(x,y,oWater);
	dir = choose(1,2,3,4)
}

instance_destroy();