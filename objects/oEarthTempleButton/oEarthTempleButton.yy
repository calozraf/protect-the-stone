{
    "id": "f769f155-c640-434b-8ff3-6c42a54016d8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEarthTempleButton",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d3daa07c-fde9-48e6-b416-9d7839a43f7d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0954af5a-2f3c-4314-80f9-63685d343f22",
    "visible": true
}