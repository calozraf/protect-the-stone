{
    "id": "bfb340e0-29fa-4a37-8f89-8e37955af634",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAirTempleButton",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d3daa07c-fde9-48e6-b416-9d7839a43f7d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0a2076ac-2392-4d06-a572-74cdfcd812ac",
    "visible": true
}