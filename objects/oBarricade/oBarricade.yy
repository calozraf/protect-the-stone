{
    "id": "2b8c0953-1bf4-4da1-beaf-8affaedc9c57",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBarricade",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b71ac85a-bd6d-4532-9535-444ad8441049",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "952b474f-a3b3-4383-99c2-8e42b135ecf0",
    "visible": true
}