{
    "id": "1bcb7633-07ec-4afe-89b5-19851baca681",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHideout",
    "eventList": [
        {
            "id": "bdbe92e3-98c3-4ae5-aeaf-232d608b8aa8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1bcb7633-07ec-4afe-89b5-19851baca681"
        },
        {
            "id": "5c8a76b6-e40d-4350-801d-1311b197cf3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1bcb7633-07ec-4afe-89b5-19851baca681"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b71ac85a-bd6d-4532-9535-444ad8441049",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0aedf572-b976-4f55-bd28-b092ffa8cb46",
    "visible": true
}