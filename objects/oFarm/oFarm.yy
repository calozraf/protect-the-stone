{
    "id": "8cebbb9b-25a2-4f42-94bd-123ed2079f82",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFarm",
    "eventList": [
        {
            "id": "6ed77575-fa63-4b8f-8412-a91396bbdda1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8cebbb9b-25a2-4f42-94bd-123ed2079f82"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7be90f0a-4e81-4621-ae0f-35651e80149b",
    "visible": true
}