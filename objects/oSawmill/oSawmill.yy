{
    "id": "f044d83e-a327-41f8-94aa-941cdf195898",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSawmill",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b71ac85a-bd6d-4532-9535-444ad8441049",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6c14f54d-b92c-4079-862f-2b6050a749f6",
    "visible": true
}