{
    "id": "c584114d-e89c-4af8-bf17-bb62672f74f8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWaterTemple",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b71ac85a-bd6d-4532-9535-444ad8441049",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "df8b5004-9ba7-4e08-990d-1b345bcfe6da",
    "visible": true
}