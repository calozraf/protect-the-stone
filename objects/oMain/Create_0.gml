/// @description Ins
#macro GRIDSIZE 32

randomize();

global.buildmode = false;
global.buildingId = 0;
global.placingObj = false;
global.flagX = -GRIDSIZE;
global.flagY = -GRIDSIZE;
global.flagIsActive = false;
/*
enum buildings {
   hideout
   }*/


room_width = 4*GRIDSIZE*20;
room_height = 3*GRIDSIZE*20;

//Create A* grid
instance_create_layer(-GRIDSIZE,-GRIDSIZE,"Instances",oPathfindingGrid);

//Spawn hideout
instance_create_layer(room_width/2,room_height/2,"Instances",oHideout);

//Proceduraly generate level
for (var i = 0; i<room_width; i+= GRIDSIZE)
{
	for (var j = 0; j<room_height; j+=GRIDSIZE)
	{
		//show_debug_message("FreeTile(" + string(i) + ", " + string(j) + ")")
		if (chance(0.95))
		{
			//Spawn a free tile
			instance_create_layer(i,j,"Instances",oFreeTile);
		}
		else
		{
			if (chance(0.8))
			{
				//Create water/tree
				if (chance(0.5))
				{
					instance_create_layer(i,j,"Instances",oWater);
				}
				else
				{
					instance_create_layer(i,j,"Instances",oTree);	
				}
			}
			else
			{
				//Create ores
				var oreGen = random(1);
				if (oreGen < 0.25)
				{
					instance_create_layer(i,j,"Instances",oTin);	
				}
				else if (oreGen < 0.5)
				{
					instance_create_layer(i,j,"Instances",oGold);
				}
				else if (oreGen < 0.75)
				{
					instance_create_layer(i,j,"Instances",oCopper);
				}
				else
				{
					instance_create_layer(i,j,"Instances",oSilver);
				}
			}
		}
	}
}


alarm[0] = 5;
show_debug_message(room_width);


//Create camera
instance_create_layer(oHideout.x,oHideout.y,"Instances",oCamera);

//Create rain particle system
instance_create_layer(0,0,"ParticleSystem",oRainCreate);

//Enumerate directions for quarry extraction
enum exdir
{
	none,
	right,
	top,
	left,
	bottom
}