{
    "id": "00e3bbac-fc06-4151-bd87-250950b735e0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMain",
    "eventList": [
        {
            "id": "f0693e64-cc26-405b-89ff-40c48c6c8056",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "00e3bbac-fc06-4151-bd87-250950b735e0"
        },
        {
            "id": "6b935129-e3d9-4172-8b1b-b628d077e831",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "00e3bbac-fc06-4151-bd87-250950b735e0"
        },
        {
            "id": "eb7e7d37-15dd-4c1b-a7b9-e4f2dec41d06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "00e3bbac-fc06-4151-bd87-250950b735e0"
        },
        {
            "id": "32ba8ce2-fa37-458b-9bf5-142a868bdc1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "00e3bbac-fc06-4151-bd87-250950b735e0"
        },
        {
            "id": "bb41403b-01ae-4ef0-9d1d-3e43e74ee4e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "00e3bbac-fc06-4151-bd87-250950b735e0"
        },
        {
            "id": "e8bc4bfc-9c64-4df5-b7da-96c09e8a4363",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "00e3bbac-fc06-4151-bd87-250950b735e0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}