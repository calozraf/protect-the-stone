{
    "id": "b458f8f3-fcef-4b8a-9c22-bb03bcb1234f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCloseUI",
    "eventList": [
        {
            "id": "2725ff10-877b-42a8-8582-963f76aae65c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b458f8f3-fcef-4b8a-9c22-bb03bcb1234f"
        },
        {
            "id": "276aeea6-96de-492a-b27b-a6c8d23bb4aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b458f8f3-fcef-4b8a-9c22-bb03bcb1234f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4142f5f5-6bd1-4cac-bb6e-59ca19bf4524",
    "visible": true
}