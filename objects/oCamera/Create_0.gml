/// @description Ins
/// @description Camera init
// You can write your code in this editor
#macro VIEW_WIDTH 448
#macro VIEW_HEIGHT 256

#macro kc keyboard_check
#macro kcp keyboard_check_pressed

instance_create_layer(0,0,"Instances",oCameraGuider);
with(oCameraGuider)
{
	event_perform(ev_create,0);
}

//global.pause = false;
shake = 0;
global.pause = false;

//Note to self : use instance_activate_region to activate part of room
//view_camera[0] = camera_create_view(0,0,320,180);


global.vx = camera_get_view_x(view_camera[0]);
global.vy = camera_get_view_y(view_camera[0]);

view_camera[0] = camera_create();
camera_set_view_size(view_camera[0],VIEW_WIDTH,VIEW_HEIGHT);
camera_set_view_pos(view_camera[0],oCameraGuider.x,oCameraGuider.y);