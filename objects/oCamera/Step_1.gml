/// @description Insert description here
// You can write your code in this editor
if (!global.pause)
{
///Camera and Screenshake
var stepsToCatchUp = 10;
var xTo,yTo;
var setCameraX, setCameraY;
//var xOffset = -64;
xTo = oCameraGuider.x //+ lengthdir_x(min(maxDistanceFromPlayer,distance_to_point(mouse_x,mouse_y)),direction);
yTo = oCameraGuider.y //+ lengthdir_y(min(maxDistanceFromPlayer*(16/9),distance_to_point(mouse_x,mouse_y)),direction)-64;
//show_debug_message(string(lengthdir_x(min(maxDistanceFromPlayer,distance_to_point(mouse_x,mouse_y)),direction)))
//y = yTo;
//x = xTo;

x += (xTo-x)/stepsToCatchUp;//it takes a maximum of 25 steps for the camera to catch up to the player
y += (yTo-y)/stepsToCatchUp;

setCameraX = -(VIEW_WIDTH/2) + x// - xOffset;
setCameraY = -(VIEW_HEIGHT/2) + y;

//Screenshake
setCameraX += random_range(-shake,shake);
setCameraY += random_range(-shake,shake);
shake *= 0.8;

//if (room == rm_shop) { setCameraX=0; setCameraY=0; }
camera_set_view_pos(view_camera[0], setCameraX, setCameraY);
//camera_set_view_pos(view_camera[0],oPlayer.x-320/2,0);

global.vx = camera_get_view_x(view_camera[0]);
global.vy = camera_get_view_y(view_camera[0]);

x = clamp(x,VIEW_WIDTH/2,room_width-VIEW_WIDTH/2);
y = clamp(y,VIEW_HEIGHT/2,room_height-VIEW_HEIGHT/2);
//Update which instances should be active
/*
show_debug_message(string(global.vx-VIEW_WIDTH)+"|"+string(global.vy-32)
					+"|"+string(global.vx+VIEW_WIDTH)+"|"+string(global.vy+32));
*/
					
//instance_activate_region(global.vx - VIEW_WIDTH/2, global.vy-32, global.vx + VIEW_WIDTH/2, VIEW_HEIGHT+32, true);
}
if kcp(ord("F"))
{
	window_set_fullscreen(!window_get_fullscreen());
}
