{
    "id": "b71ac85a-bd6d-4532-9535-444ad8441049",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBuilding",
    "eventList": [
        {
            "id": "a5f9afdd-60db-4eae-8369-efd7415ccde0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b71ac85a-bd6d-4532-9535-444ad8441049"
        },
        {
            "id": "399943a3-081f-44a6-971c-9f4266f2eb5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b71ac85a-bd6d-4532-9535-444ad8441049"
        },
        {
            "id": "3b1b702c-df3e-4b46-b806-2e6f73abd308",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b71ac85a-bd6d-4532-9535-444ad8441049"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4cf6f271-109e-40d8-9337-15d7a8870aef",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}