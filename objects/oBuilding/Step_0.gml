/// @description Ins
mOver = mouse_over();

if (mOver)
{
	if (mouse_check_button_pressed(mb_left))
	{
		if (!global.placingObj)
		{
			if (!global.buildmode || global.buildingId != id)
			{
				oSelector.buildingSprite = -1;
				global.buildmode = true;
				global.activeBuilding = object_index;
				instance_destroy(oButton);
				oUIBox.buildingId = id;
				set_ui(object_index);
			}
		}
	}
}