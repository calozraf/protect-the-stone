///@function spr_to_obj
///@param sprite_index

switch (argument0)
{
	case sQuarry : return oQuarry;
	case sConveyerBelt : return oConveyerBelt;
	case sSawmill : return oSawmill;
	case sWell : return oWell;
	case sStables : return oStables;
	case sStoneWall : return oBarricade;
	case sFarm : return oFarm;
	case sAirTemple : return oAirTemple;
	case sEarthTemple : return oEarthTemple;
	case sFireTemple : return oFireTemple;
	case sWaterTemple : return oWaterTemple;
	case sRessourceCrosser : return oRessourceCrosser;
	
	default : return oConveyerBelt;
}