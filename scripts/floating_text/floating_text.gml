///@function floating_text(x,y,string,col)
///@param x
///@param y
///@param string
///@param col

var i = instance_create_depth(argument0,argument1,-10002,oTextFloat);
i.text = argument2;
i.image_blend = argument3;