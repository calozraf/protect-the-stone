///@function create_bucket
///@param extractionInstId
///@param conveyerBeltId
var bucket = instance_create_layer(argument1.x+16,argument1.y+16,"Instances",oBucket);

switch (argument0.object_index)
{
	case oTin : 
		bucket.image_index = 0;
		break;
	case oSilver : 
		bucket.image_index = 1;
		break;
	case oCopper : 
		bucket.image_index = 2;
		break;
	case oGold : 
		bucket.image_index = 3;
		break;
}