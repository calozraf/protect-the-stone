///@function set_desc
///@param object_index

with(oBoxDesc)
{
	switch (argument0)
	{
		case oQuarryButton : 
			itemName = "Quarry";
			itemDesc = "This building can excavate metals if built near them."
			break;
		case oSawmillButton : 
			itemName = "Sawmill";
			itemDesc = "This building can gather wood if placed near trees."
			break;
		case oFarmButton : 
			itemName = "Farm";
			itemDesc = "Used to harvest the food required to feed troops."
			break;
		case oBarricadeButton : 
			itemName = "Barricade";
			itemDesc = "Surround your buildings with these to protect them."
			break;
		case oWellButton : 
			itemName = "Well";
			itemDesc = "This building gathers water if placed near a source of it."
			break;
		case oConveyerBeltButton : 
			itemName = "Conveyer Belt";
			itemDesc = "Build a pipeline of conveyer belts to get ressources to their respective alchemy stations."
			break;
		case oEarthTempleButton : 
			itemName = "Earth temple";
			itemDesc = "Feed it copper and gold to summon monsters";
			break;
		case oAirTempleButton : 
			itemName = "Air temple";
			itemDesc = "Feed it gold and silver to summon monsters";
			break;
		case oWaterTempleButton : 
			itemName = "Water temple";
			itemDesc = "Feed it silver and lead to summon monsters";
			break;
		case oFireTempleButton : 
			itemName = "Fire temple";
			itemDesc = "Feed it lead and copper to summon monsters";
			break;
		case oStablesButton :
			itemName = "Stables";
			itemDesc = "Increases population by 3";
			break;
		case oRessourceCrosserButton :
			itemName = "Ressource Crosser";
			itemDesc = "Allows you to cross ressource pipelines";
			break;
		default:
			break;
	}
}