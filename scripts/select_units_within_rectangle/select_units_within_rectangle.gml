///@function select_units_within_rectangle
var rx = oSelector.rx;
var ry = oSelector.ry;
with (oChimera)
{
	selected = false;
	var xWithin, yWithin;
	
	if (rx < mouse_x) xWithin = within(x,rx,mouse_x);
	else xWithin = within(x,mouse_x,rx);
	
	if (ry < mouse_y) yWithin = within(y,ry,mouse_y);
	else yWithin = within(y,mouse_y,ry);
	
	if (xWithin && yWithin)
	{
		selected = true;	
	}
}