///@function set_ui
///@param object_index
ui_flush();

with(oUIBox)
{
	switch (argument0)
	{
		case oHideout : 
			buildingName = "Alchemist's Hideout";
			obj[0] = oConveyerBeltButton;
			obj[1] = oSawmillButton;
			obj[2] = oQuarryButton;
			obj[3] = oWellButton;
			obj[4] = oStablesButton;
			obj[5] = oBarricadeButton;
			obj[6] = oFarmButton;
			obj[7] = oAirTempleButton;
			obj[8] = oEarthTempleButton;
			obj[9] = oFireTempleButton;
			obj[10] = oWaterTempleButton;
			obj[11] = oRessourceCrosserButton;
			
			//event_perform_object(oUIBox,ev_step,0);
			
			for (var i = 0; i<6; i++)
			{
				instance_create_depth(i*36-ogX+2,ogY+16+2,-101,obj[i]);
			}
			for (var i = 6; i<12; i++)
			{
				instance_create_depth(i*36-ogX-36*6+2,ogY+16+4+32,-101,obj[i]);
			}
			break;
			
		case oSawmill : 
			buildingName = "Sawmill";
			obj[0] = oDestroyButton;
			instance_create_depth(-ogX+2,ogY+16+2,-101,obj[0]);
			break;
			
		case oWaterTemple : 
			buildingName = "Water temple";
			obj[0] = oDestroyButton;
			instance_create_depth(-ogX+2,ogY+16+2,-101,obj[0]);
			break;
			
		case oAirTemple : 
			buildingName = "Air temple";
			obj[0] = oDestroyButton;
			instance_create_depth(-ogX+2,ogY+16+2,-101,obj[0]);
			break;
			
		case oEarthTemple : 
			buildingName = "Earth temple";
			obj[0] = oDestroyButton;
			instance_create_depth(-ogX+2,ogY+16+2,-101,obj[0]);
			break;
			
		case oFireTemple : 
			buildingName = "Fire temple";
			obj[0] = oDestroyButton;
			instance_create_depth(-ogX+2,ogY+16+2,-101,obj[0]);
			break;
			
		case oFarm : 
			buildingName = "Farm";
			obj[0] = oDestroyButton;
			instance_create_depth(-ogX+2,ogY+16+2,-101,obj[0]);
			break;
			
		case oQuarry : 
			buildingName = "Quarry";
			obj[0] = oDestroyButton;
			instance_create_depth(-ogX+2,ogY+16+2,-101,obj[0]);
			break;
			
		case oWell : 
			buildingName = "Well";
			obj[0] = oDestroyButton;
			instance_create_depth(-ogX+2,ogY+16+2,-101,obj[0]);
			break;
			
		case oRessourceCrosser : 
			buildingName = "Ressource crosser";
			obj[0] = oDestroyButton;
			instance_create_depth(-ogX+2,ogY+16+2,-101,obj[0]);
			break;
			
		case oTin:
			buildingName = "Tin Patch";
			break;
			
		case oCopper:
			buildingName = "Copper Patch";
			break;
			
		case oSilver:
			buildingName = "Silver Patch";
			break;
		
		case oGold:
			buildingName = "Gold Patch";
			break;
			
		case oWater:
			buildingName = "Water";
			break;
			
		case oTree:
			buildingName = "Tree";
			break;
			
		default:
			buildingName = object_get_name(argument0);
			obj[0] = oDestroyButton;
			for (var i = 0; i<1; i++)
			{
				instance_create_depth(i*36-ogX+2,ogY+16+2,-101,obj[i]);
			}
			break;
			
	}
}