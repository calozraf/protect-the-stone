{
    "id": "0aedf572-b976-4f55-bd28-b092ffa8cb46",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHideout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 5,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a57d5796-8e78-4da8-a842-1997391f9781",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aedf572-b976-4f55-bd28-b092ffa8cb46",
            "compositeImage": {
                "id": "55144e25-eca6-4d39-8107-2ffbb0e933df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a57d5796-8e78-4da8-a842-1997391f9781",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f0d364a-f4d4-4ec1-9d52-869b9e75a180",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a57d5796-8e78-4da8-a842-1997391f9781",
                    "LayerId": "b942820c-426c-405e-8c73-fd8d5bcd1618"
                }
            ]
        },
        {
            "id": "bece76c9-32e0-4312-a4cb-e3eb9bbb8efa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aedf572-b976-4f55-bd28-b092ffa8cb46",
            "compositeImage": {
                "id": "a4d55c8a-820f-48e0-9e18-925a952ee961",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bece76c9-32e0-4312-a4cb-e3eb9bbb8efa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dd1b5b4-79f8-4b75-95f5-71685f9f5a88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bece76c9-32e0-4312-a4cb-e3eb9bbb8efa",
                    "LayerId": "b942820c-426c-405e-8c73-fd8d5bcd1618"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b942820c-426c-405e-8c73-fd8d5bcd1618",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0aedf572-b976-4f55-bd28-b092ffa8cb46",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}