{
    "id": "14ca9bd0-94e7-4e28-b6af-f104fd56a96c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sres",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b754fec3-09c0-49e0-81a6-b0403e7020db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14ca9bd0-94e7-4e28-b6af-f104fd56a96c",
            "compositeImage": {
                "id": "b992eb87-86e2-4d98-9cc0-9dc0ac6fb869",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b754fec3-09c0-49e0-81a6-b0403e7020db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f364297f-5c89-4c17-847a-3a349d58dce9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b754fec3-09c0-49e0-81a6-b0403e7020db",
                    "LayerId": "8451e945-0bed-4976-814b-2f46a7cd9f54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8451e945-0bed-4976-814b-2f46a7cd9f54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14ca9bd0-94e7-4e28-b6af-f104fd56a96c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}