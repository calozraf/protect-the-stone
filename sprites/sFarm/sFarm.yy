{
    "id": "7be90f0a-4e81-4621-ae0f-35651e80149b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFarm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee0d94e8-f83a-46ad-b140-aafa4174c747",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7be90f0a-4e81-4621-ae0f-35651e80149b",
            "compositeImage": {
                "id": "a5a8c456-9b59-4059-be27-e190c4d6476c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee0d94e8-f83a-46ad-b140-aafa4174c747",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7916a9ab-f06f-4a93-977f-666744d322de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee0d94e8-f83a-46ad-b140-aafa4174c747",
                    "LayerId": "06a9d558-e3d1-4bd7-a0ce-161aa0034f6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "06a9d558-e3d1-4bd7-a0ce-161aa0034f6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7be90f0a-4e81-4621-ae0f-35651e80149b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}