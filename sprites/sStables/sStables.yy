{
    "id": "06f43e4d-9c7c-464e-8e39-84fc5f06a003",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStables",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0663909a-029e-472e-8163-8a2f61b08e3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06f43e4d-9c7c-464e-8e39-84fc5f06a003",
            "compositeImage": {
                "id": "ce42a432-898b-4060-b539-f70b55aec8ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0663909a-029e-472e-8163-8a2f61b08e3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f330e14d-2f30-4e85-9529-d24d727a18b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0663909a-029e-472e-8163-8a2f61b08e3c",
                    "LayerId": "52e76378-5c8f-4c61-bdfe-171d372c08be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "52e76378-5c8f-4c61-bdfe-171d372c08be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06f43e4d-9c7c-464e-8e39-84fc5f06a003",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}