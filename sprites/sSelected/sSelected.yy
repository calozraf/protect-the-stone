{
    "id": "80834387-625a-457f-b80b-cea86982fe57",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSelected",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf3c0324-8b06-4821-8106-d7a3d8d90502",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80834387-625a-457f-b80b-cea86982fe57",
            "compositeImage": {
                "id": "0e31fa0c-6d8a-4a00-bc50-19ef0198f7cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf3c0324-8b06-4821-8106-d7a3d8d90502",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8095da17-298b-4660-ba54-ea0ab5e97b04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf3c0324-8b06-4821-8106-d7a3d8d90502",
                    "LayerId": "eb217192-c3ca-4af7-84d6-66500963c934"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "eb217192-c3ca-4af7-84d6-66500963c934",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "80834387-625a-457f-b80b-cea86982fe57",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}