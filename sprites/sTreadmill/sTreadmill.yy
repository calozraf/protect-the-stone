{
    "id": "2ce2167f-4ca0-44a0-8cf3-4733f08ded4a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTreadmill",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed03c937-0482-4ae6-848d-f14217dd1852",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce2167f-4ca0-44a0-8cf3-4733f08ded4a",
            "compositeImage": {
                "id": "78071ab5-4dc6-4346-bbf5-778603ed26fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed03c937-0482-4ae6-848d-f14217dd1852",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fde98a88-12f4-4d12-9ced-ed21b19c7076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed03c937-0482-4ae6-848d-f14217dd1852",
                    "LayerId": "ffbea197-eaf3-4e70-8726-72f646cf6425"
                }
            ]
        },
        {
            "id": "d53eedc0-bb49-4829-ada0-3c489c7eda18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ce2167f-4ca0-44a0-8cf3-4733f08ded4a",
            "compositeImage": {
                "id": "51e6b614-7aee-41fd-aec6-bfb8936c69c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d53eedc0-bb49-4829-ada0-3c489c7eda18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "707d1772-a042-46ae-9bc1-50d5f341be26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d53eedc0-bb49-4829-ada0-3c489c7eda18",
                    "LayerId": "ffbea197-eaf3-4e70-8726-72f646cf6425"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ffbea197-eaf3-4e70-8726-72f646cf6425",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ce2167f-4ca0-44a0-8cf3-4733f08ded4a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}