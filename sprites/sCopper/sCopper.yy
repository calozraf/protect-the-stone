{
    "id": "dd7cb858-d092-4d5c-8f4b-c2af9dd70579",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCopper",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b74e203-8db0-4a14-a4d2-e3ff440d15d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd7cb858-d092-4d5c-8f4b-c2af9dd70579",
            "compositeImage": {
                "id": "45a9515a-3783-417f-bc6c-b5b58b464d5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b74e203-8db0-4a14-a4d2-e3ff440d15d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5b46469-b34c-4a0a-ad01-34b959214eaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b74e203-8db0-4a14-a4d2-e3ff440d15d1",
                    "LayerId": "a9e2b2d4-d82c-4692-85f9-55cfceedd239"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a9e2b2d4-d82c-4692-85f9-55cfceedd239",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd7cb858-d092-4d5c-8f4b-c2af9dd70579",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}