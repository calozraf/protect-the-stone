{
    "id": "fdbb2368-d131-4abf-a842-969e3bbd92cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWell",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 4,
    "bbox_right": 29,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "793d5e42-63f8-4ae0-93cb-61780c287d4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdbb2368-d131-4abf-a842-969e3bbd92cf",
            "compositeImage": {
                "id": "f8a01087-a361-44b0-b93d-7c4ba9da1441",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "793d5e42-63f8-4ae0-93cb-61780c287d4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "328e9c44-40a4-47d2-8416-813e6b67d060",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "793d5e42-63f8-4ae0-93cb-61780c287d4d",
                    "LayerId": "650495b3-da61-4bac-a0ba-a176add186cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "650495b3-da61-4bac-a0ba-a176add186cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fdbb2368-d131-4abf-a842-969e3bbd92cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}