{
    "id": "328915da-8a71-483d-93e1-c3c48cacdb23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBucket",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4491495b-bbf2-4cae-9b34-257ab86225c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "328915da-8a71-483d-93e1-c3c48cacdb23",
            "compositeImage": {
                "id": "74ff2c54-19bf-40c2-84a9-68d811066c1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4491495b-bbf2-4cae-9b34-257ab86225c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b349ad85-b8be-47df-bf2b-aa701b7c902c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4491495b-bbf2-4cae-9b34-257ab86225c8",
                    "LayerId": "4d5d5fbd-619e-4eb4-b278-e84228c81a40"
                }
            ]
        },
        {
            "id": "b210d9fb-5cfe-4048-886b-c9e96072a39c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "328915da-8a71-483d-93e1-c3c48cacdb23",
            "compositeImage": {
                "id": "dbb7d1c7-b550-462a-8a01-f8381a025308",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b210d9fb-5cfe-4048-886b-c9e96072a39c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "891711ed-75ad-43dd-b392-e3e760fe42b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b210d9fb-5cfe-4048-886b-c9e96072a39c",
                    "LayerId": "4d5d5fbd-619e-4eb4-b278-e84228c81a40"
                }
            ]
        },
        {
            "id": "2840366f-1827-4a35-b0f3-c2e188bbcdbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "328915da-8a71-483d-93e1-c3c48cacdb23",
            "compositeImage": {
                "id": "ddf24b57-eec3-41bd-a34f-f7e2c9a1eea5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2840366f-1827-4a35-b0f3-c2e188bbcdbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd0d5f32-26a6-4c0d-9197-0aee26502740",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2840366f-1827-4a35-b0f3-c2e188bbcdbe",
                    "LayerId": "4d5d5fbd-619e-4eb4-b278-e84228c81a40"
                }
            ]
        },
        {
            "id": "6f793a36-1165-4159-9304-1ba264a82dbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "328915da-8a71-483d-93e1-c3c48cacdb23",
            "compositeImage": {
                "id": "e2dd146b-7ff9-4ea9-a791-98f5b83d7e3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f793a36-1165-4159-9304-1ba264a82dbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8110408e-f81d-4578-b00c-a1744c74c8fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f793a36-1165-4159-9304-1ba264a82dbb",
                    "LayerId": "4d5d5fbd-619e-4eb4-b278-e84228c81a40"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4d5d5fbd-619e-4eb4-b278-e84228c81a40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "328915da-8a71-483d-93e1-c3c48cacdb23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}