{
    "id": "7eb320ad-c1e0-432a-ad15-84c089459699",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_freezone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f76c6610-37c5-49ee-a919-9966239f9420",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7eb320ad-c1e0-432a-ad15-84c089459699",
            "compositeImage": {
                "id": "30769316-d41e-4497-bd3c-13bfcdcb78e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f76c6610-37c5-49ee-a919-9966239f9420",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e05c5e55-21f5-40ca-8ff4-2c4bfb1098f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f76c6610-37c5-49ee-a919-9966239f9420",
                    "LayerId": "c4e88029-dd61-40e0-b02c-065f80be4c60"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c4e88029-dd61-40e0-b02c-065f80be4c60",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7eb320ad-c1e0-432a-ad15-84c089459699",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}