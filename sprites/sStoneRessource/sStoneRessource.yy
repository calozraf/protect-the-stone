{
    "id": "c06ac8bb-6016-4862-abdb-66aa6b8504d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStoneRessource",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f124ad1b-1bdb-4062-af53-ff0fdc54397e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c06ac8bb-6016-4862-abdb-66aa6b8504d4",
            "compositeImage": {
                "id": "563fd3a5-e91c-440f-b30b-2f1c2e79056c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f124ad1b-1bdb-4062-af53-ff0fdc54397e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25babdd1-48b3-4525-bf15-1189c681f120",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f124ad1b-1bdb-4062-af53-ff0fdc54397e",
                    "LayerId": "10ffce2a-baee-494f-8c6d-5f60ea524cff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "10ffce2a-baee-494f-8c6d-5f60ea524cff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c06ac8bb-6016-4862-abdb-66aa6b8504d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}