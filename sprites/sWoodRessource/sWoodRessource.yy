{
    "id": "0b0382c0-6d3c-4a21-954b-3cf36b744303",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWoodRessource",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6de070d1-cef0-4ec1-9e7f-2ad177b1e13f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b0382c0-6d3c-4a21-954b-3cf36b744303",
            "compositeImage": {
                "id": "c16568db-ce05-4d5d-94d7-548aaac7d8bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6de070d1-cef0-4ec1-9e7f-2ad177b1e13f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "149fef53-5122-45a0-8489-3d2e89ffbdd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6de070d1-cef0-4ec1-9e7f-2ad177b1e13f",
                    "LayerId": "871f8f69-ba93-4c20-9cf1-a462579b1d46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "871f8f69-ba93-4c20-9cf1-a462579b1d46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b0382c0-6d3c-4a21-954b-3cf36b744303",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}