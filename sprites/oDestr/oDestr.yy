{
    "id": "34ea282b-87c7-41e9-9183-e31d24ccce01",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "oDestr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 7,
    "bbox_right": 27,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "898142d2-5bec-4028-912c-2d2d027f741c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34ea282b-87c7-41e9-9183-e31d24ccce01",
            "compositeImage": {
                "id": "523a65de-9881-42ab-8113-90a0baadebcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "898142d2-5bec-4028-912c-2d2d027f741c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b9c2bb2-d1a7-4a0a-8b5c-f6809f84f1a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "898142d2-5bec-4028-912c-2d2d027f741c",
                    "LayerId": "f6787b1b-4cf7-4360-a329-97c71f955686"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f6787b1b-4cf7-4360-a329-97c71f955686",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34ea282b-87c7-41e9-9183-e31d24ccce01",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}