{
    "id": "e876745a-81c6-427d-98f2-22b5b4235805",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sConveyerBelt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "68858c42-4a0b-4705-bc33-7aa1736fb383",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e876745a-81c6-427d-98f2-22b5b4235805",
            "compositeImage": {
                "id": "ea7b444e-28fc-4ef3-a18c-d05a427dca69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68858c42-4a0b-4705-bc33-7aa1736fb383",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2609f15f-88ab-4021-a73e-d0054432e20e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68858c42-4a0b-4705-bc33-7aa1736fb383",
                    "LayerId": "cc38cefd-248b-4b40-a803-cd038e4536a5"
                }
            ]
        },
        {
            "id": "38d50118-dec6-4de4-85b2-08cdd1b50e36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e876745a-81c6-427d-98f2-22b5b4235805",
            "compositeImage": {
                "id": "88a56059-caa9-4edd-8a33-74088e79ea88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38d50118-dec6-4de4-85b2-08cdd1b50e36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88cc5d85-9611-4082-ab75-859b61772a81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38d50118-dec6-4de4-85b2-08cdd1b50e36",
                    "LayerId": "cc38cefd-248b-4b40-a803-cd038e4536a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cc38cefd-248b-4b40-a803-cd038e4536a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e876745a-81c6-427d-98f2-22b5b4235805",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}