{
    "id": "6c14f54d-b92c-4079-862f-2b6050a749f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSawmill",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33b93614-e788-41e4-9b5d-d0a0bfed6d54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c14f54d-b92c-4079-862f-2b6050a749f6",
            "compositeImage": {
                "id": "c5acffa6-c16f-41ca-93c9-0ff6f42f0977",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33b93614-e788-41e4-9b5d-d0a0bfed6d54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39cabfc4-be1e-49e9-b87e-d2f713898382",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33b93614-e788-41e4-9b5d-d0a0bfed6d54",
                    "LayerId": "df6a6cf1-0924-4d0d-a40c-485d209158ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "df6a6cf1-0924-4d0d-a40c-485d209158ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c14f54d-b92c-4079-862f-2b6050a749f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}