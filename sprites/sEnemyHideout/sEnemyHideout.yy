{
    "id": "1ef4feed-6a42-4ff6-b1ea-6be2bd453af5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyHideout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3750dd77-9c49-4aca-860a-b768d49f3594",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ef4feed-6a42-4ff6-b1ea-6be2bd453af5",
            "compositeImage": {
                "id": "e2dc52d5-e514-48a8-a0a5-0fb8848aea86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3750dd77-9c49-4aca-860a-b768d49f3594",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97b52d0c-d49e-47c1-af68-b4faa65a3252",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3750dd77-9c49-4aca-860a-b768d49f3594",
                    "LayerId": "f69c30c0-3674-424a-95fb-d0740ecbeb15"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f69c30c0-3674-424a-95fb-d0740ecbeb15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ef4feed-6a42-4ff6-b1ea-6be2bd453af5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}