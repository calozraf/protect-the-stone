{
    "id": "45756a4f-c4fe-4f7c-aea3-28981e217a26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWaterRessource",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7bd2baa0-63c0-487d-a3ad-372fd316ffbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45756a4f-c4fe-4f7c-aea3-28981e217a26",
            "compositeImage": {
                "id": "a869255a-42b2-4fc2-8f95-9ca31e7e715a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bd2baa0-63c0-487d-a3ad-372fd316ffbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1136478a-e0e2-4a7b-997f-574ed1f30d2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bd2baa0-63c0-487d-a3ad-372fd316ffbc",
                    "LayerId": "82cd48b1-29d3-4184-a988-8aae94e3c6e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "82cd48b1-29d3-4184-a988-8aae94e3c6e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "45756a4f-c4fe-4f7c-aea3-28981e217a26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}