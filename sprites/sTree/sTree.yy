{
    "id": "773a6b3c-62f6-4544-9cb1-39e72d293b4e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7046b643-04f1-45eb-bbd8-9886380e28ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "773a6b3c-62f6-4544-9cb1-39e72d293b4e",
            "compositeImage": {
                "id": "021d066c-2606-48d3-9f27-159afe32fe49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7046b643-04f1-45eb-bbd8-9886380e28ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "038adb1d-5f1a-4c03-a9fc-cb007c355a9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7046b643-04f1-45eb-bbd8-9886380e28ac",
                    "LayerId": "f51395f5-7f8c-483e-8963-b86d7ac27438"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f51395f5-7f8c-483e-8963-b86d7ac27438",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "773a6b3c-62f6-4544-9cb1-39e72d293b4e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}