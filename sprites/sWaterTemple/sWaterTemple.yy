{
    "id": "df8b5004-9ba7-4e08-990d-1b345bcfe6da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWaterTemple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 2,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "207abf72-33b2-4764-a607-a1d0a1f9db0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df8b5004-9ba7-4e08-990d-1b345bcfe6da",
            "compositeImage": {
                "id": "d0320909-4449-497a-a1d1-66c3994c9dbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "207abf72-33b2-4764-a607-a1d0a1f9db0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70877d0b-26bf-4785-8f45-d3b3ef4db884",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "207abf72-33b2-4764-a607-a1d0a1f9db0d",
                    "LayerId": "96d2471a-2f02-4ebd-a1a1-86cc959611a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "96d2471a-2f02-4ebd-a1a1-86cc959611a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df8b5004-9ba7-4e08-990d-1b345bcfe6da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}