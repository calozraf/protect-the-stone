{
    "id": "ca6f9f5d-b9b1-4d60-81e8-03bf370de1a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBoxUI",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 223,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6246c05f-1e32-4ea1-8b1e-5a2d3e3fc2b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca6f9f5d-b9b1-4d60-81e8-03bf370de1a6",
            "compositeImage": {
                "id": "4013054a-bd57-463d-a631-2de21d7bfc3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6246c05f-1e32-4ea1-8b1e-5a2d3e3fc2b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f2da790-18d7-445e-bccf-06916738fb91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6246c05f-1e32-4ea1-8b1e-5a2d3e3fc2b7",
                    "LayerId": "1528e482-dd64-45ec-a811-8451b5e1c500"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "1528e482-dd64-45ec-a811-8451b5e1c500",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca6f9f5d-b9b1-4d60-81e8-03bf370de1a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 224,
    "xorig": 0,
    "yorig": 0
}