{
    "id": "0a2076ac-2392-4d06-a572-74cdfcd812ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAirTemple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 2,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c04b3fa9-b463-44b2-97cd-96e94cc2bf98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a2076ac-2392-4d06-a572-74cdfcd812ac",
            "compositeImage": {
                "id": "bab32cb1-c3e5-4bdd-8698-04ef8ba8fe58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c04b3fa9-b463-44b2-97cd-96e94cc2bf98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee9c274f-ae40-48ee-b7f4-38af28bce22a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c04b3fa9-b463-44b2-97cd-96e94cc2bf98",
                    "LayerId": "8bc02b1b-c431-4317-ae19-638245f1e983"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8bc02b1b-c431-4317-ae19-638245f1e983",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0a2076ac-2392-4d06-a572-74cdfcd812ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}