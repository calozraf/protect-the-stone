{
    "id": "4ab1afb2-4de3-4012-93fb-8d502a2af1af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBoxDesc",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 223,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7de5ea4c-d1a0-45ad-808c-49769611c7e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ab1afb2-4de3-4012-93fb-8d502a2af1af",
            "compositeImage": {
                "id": "cc95f5d8-6c61-4472-a6bb-299052b6e2bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7de5ea4c-d1a0-45ad-808c-49769611c7e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29295b19-d32a-4a41-952f-1e9ca47fee0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7de5ea4c-d1a0-45ad-808c-49769611c7e6",
                    "LayerId": "870aae4b-d969-4ae4-bd58-0039d985be2d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "870aae4b-d969-4ae4-bd58-0039d985be2d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ab1afb2-4de3-4012-93fb-8d502a2af1af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 224,
    "xorig": 0,
    "yorig": 0
}