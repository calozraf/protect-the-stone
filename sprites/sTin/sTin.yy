{
    "id": "dd773770-4c2a-4761-9449-c9d5ef7be04e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53312aa5-de57-44d7-891e-015f7d921b90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd773770-4c2a-4761-9449-c9d5ef7be04e",
            "compositeImage": {
                "id": "f6236248-9ff0-4bd9-81c2-fb7be0cc218f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53312aa5-de57-44d7-891e-015f7d921b90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bd9b0ec-4d4c-49de-8dea-0c51545e1170",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53312aa5-de57-44d7-891e-015f7d921b90",
                    "LayerId": "412b7e5d-7f6d-41db-90fd-02d7114a5fed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "412b7e5d-7f6d-41db-90fd-02d7114a5fed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd773770-4c2a-4761-9449-c9d5ef7be04e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}