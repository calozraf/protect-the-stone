{
    "id": "cc80e866-8f12-4d90-bd13-4f2c05b38899",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFlag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6818eb15-db24-4444-a5a0-187266d0a8d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc80e866-8f12-4d90-bd13-4f2c05b38899",
            "compositeImage": {
                "id": "154531f8-7917-417f-b3ce-6ddfb09453da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6818eb15-db24-4444-a5a0-187266d0a8d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee3aeb58-8398-47eb-b4ce-202ccc724e0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6818eb15-db24-4444-a5a0-187266d0a8d4",
                    "LayerId": "53d6f054-b2fc-4d91-878d-f4631363cc42"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "53d6f054-b2fc-4d91-878d-f4631363cc42",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc80e866-8f12-4d90-bd13-4f2c05b38899",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 37
}