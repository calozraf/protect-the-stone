{
    "id": "ff6533c5-7112-451b-995f-3d842df40951",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGrass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca008683-2e10-4e47-babd-4f870b224df0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff6533c5-7112-451b-995f-3d842df40951",
            "compositeImage": {
                "id": "83db7895-b26b-42b1-a6cf-f25638ddbb43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca008683-2e10-4e47-babd-4f870b224df0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81418543-d471-4ddf-977e-7407029575ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca008683-2e10-4e47-babd-4f870b224df0",
                    "LayerId": "23aaeb96-f6fb-48a5-8296-b9cd7736b230"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "23aaeb96-f6fb-48a5-8296-b9cd7736b230",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff6533c5-7112-451b-995f-3d842df40951",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}