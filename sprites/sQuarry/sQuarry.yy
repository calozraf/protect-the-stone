{
    "id": "69d1613e-2b35-4115-bdaa-4c662c2831dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sQuarry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5822b17-4fd0-40db-a8b0-68536cc523f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69d1613e-2b35-4115-bdaa-4c662c2831dc",
            "compositeImage": {
                "id": "6be67569-dece-46d5-8e76-a0f3065bfeed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5822b17-4fd0-40db-a8b0-68536cc523f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5a5da28-3749-4e0a-8185-65c32b7ce89b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5822b17-4fd0-40db-a8b0-68536cc523f9",
                    "LayerId": "345b3acb-6b94-42a8-a2b5-1535f1468b40"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "345b3acb-6b94-42a8-a2b5-1535f1468b40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "69d1613e-2b35-4115-bdaa-4c662c2831dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}