{
    "id": "0954af5a-2f3c-4314-80f9-63685d343f22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFireTemple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 2,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7367703f-362b-4fd1-9727-795ed1ce7648",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0954af5a-2f3c-4314-80f9-63685d343f22",
            "compositeImage": {
                "id": "11829783-069c-4713-ad3d-7e5b1f56e069",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7367703f-362b-4fd1-9727-795ed1ce7648",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "096198e3-3a02-4425-81da-cc0d0c8374c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7367703f-362b-4fd1-9727-795ed1ce7648",
                    "LayerId": "9bf8cc45-b4b5-47bb-b8cc-6e687032bcd3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9bf8cc45-b4b5-47bb-b8cc-6e687032bcd3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0954af5a-2f3c-4314-80f9-63685d343f22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}