{
    "id": "338369e0-7cac-41b3-b95f-1c0e9d7aa03d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPowerIcon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c46e86ed-4cd5-4542-bd98-3fb1af4255a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "338369e0-7cac-41b3-b95f-1c0e9d7aa03d",
            "compositeImage": {
                "id": "c05fe788-9c90-42ce-a712-c4631cbab8b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c46e86ed-4cd5-4542-bd98-3fb1af4255a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8fe5137-134c-4ebd-8133-3850137c056b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c46e86ed-4cd5-4542-bd98-3fb1af4255a0",
                    "LayerId": "ff688c3b-17b0-4637-b71c-f2a2677c4bdd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "ff688c3b-17b0-4637-b71c-f2a2677c4bdd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "338369e0-7cac-41b3-b95f-1c0e9d7aa03d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}