{
    "id": "36c417b2-abe7-42d0-befe-321f7bd4ee8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWater",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e2cd9b1-66c6-4cdf-9247-6957d49c2630",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36c417b2-abe7-42d0-befe-321f7bd4ee8d",
            "compositeImage": {
                "id": "06a662d8-5c1d-4e18-98de-d51e9e7083d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e2cd9b1-66c6-4cdf-9247-6957d49c2630",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fe5d646-8ca0-4afc-97d7-64665dd61f5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e2cd9b1-66c6-4cdf-9247-6957d49c2630",
                    "LayerId": "db7fc126-0f61-4b52-8e37-8d193500100b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "db7fc126-0f61-4b52-8e37-8d193500100b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36c417b2-abe7-42d0-befe-321f7bd4ee8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}