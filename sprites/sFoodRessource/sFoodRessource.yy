{
    "id": "017e8fdd-4361-4945-816d-dc464e7603dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFoodRessource",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "686cc455-5e4b-452b-9b8d-b8f5813b1774",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "017e8fdd-4361-4945-816d-dc464e7603dc",
            "compositeImage": {
                "id": "42fed06b-b204-4510-9037-aea4ac2db43b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "686cc455-5e4b-452b-9b8d-b8f5813b1774",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8db12849-8e0c-4eee-9042-bc58c377a9f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "686cc455-5e4b-452b-9b8d-b8f5813b1774",
                    "LayerId": "7287b71d-56d5-404f-ba88-31ea20de3e43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "7287b71d-56d5-404f-ba88-31ea20de3e43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "017e8fdd-4361-4945-816d-dc464e7603dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}