{
    "id": "7adf1610-452f-4d93-af23-b320ee60d5db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sS",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "898cea15-a6cc-4360-a7df-e0576a76479b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7adf1610-452f-4d93-af23-b320ee60d5db",
            "compositeImage": {
                "id": "bdf71e7b-cf21-4b9d-a49c-f836fb0b1f93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "898cea15-a6cc-4360-a7df-e0576a76479b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d767a444-f627-4816-9faa-5838fd17919c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "898cea15-a6cc-4360-a7df-e0576a76479b",
                    "LayerId": "bc6725ae-fd29-4813-bddf-040a55112ea2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bc6725ae-fd29-4813-bddf-040a55112ea2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7adf1610-452f-4d93-af23-b320ee60d5db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}