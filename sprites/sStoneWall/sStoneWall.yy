{
    "id": "952b474f-a3b3-4383-99c2-8e42b135ecf0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStoneWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c572ff78-e352-4c32-af5c-10e334a46bc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "952b474f-a3b3-4383-99c2-8e42b135ecf0",
            "compositeImage": {
                "id": "ee6590a3-eaf2-4d1f-bbcc-62da7326c37f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c572ff78-e352-4c32-af5c-10e334a46bc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaf65a3f-bfca-408e-ab96-838af814712c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c572ff78-e352-4c32-af5c-10e334a46bc2",
                    "LayerId": "76f941e6-5781-4a4c-9bb7-1fe6887b28b9"
                }
            ]
        },
        {
            "id": "6e0a8656-06cf-48d0-8401-650355f743c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "952b474f-a3b3-4383-99c2-8e42b135ecf0",
            "compositeImage": {
                "id": "901e534d-8e0a-4f70-8a96-5f9d543d9b88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e0a8656-06cf-48d0-8401-650355f743c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0119cb6a-290f-4210-b20c-da5f2ecdd070",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e0a8656-06cf-48d0-8401-650355f743c6",
                    "LayerId": "76f941e6-5781-4a4c-9bb7-1fe6887b28b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "76f941e6-5781-4a4c-9bb7-1fe6887b28b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "952b474f-a3b3-4383-99c2-8e42b135ecf0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}