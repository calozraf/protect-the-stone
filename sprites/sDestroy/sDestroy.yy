{
    "id": "279d6d50-1399-42fd-9fda-deda14ddb4d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDestroy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76fbbf26-92aa-427f-8cab-e4a5eaa95eaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "279d6d50-1399-42fd-9fda-deda14ddb4d6",
            "compositeImage": {
                "id": "ae962da8-777e-47c1-8e52-e5d77a58c69b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76fbbf26-92aa-427f-8cab-e4a5eaa95eaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68852c1a-b33f-484d-802f-bfc74b9b8056",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76fbbf26-92aa-427f-8cab-e4a5eaa95eaa",
                    "LayerId": "c439d0d6-def3-4e1c-a062-20413d96e4fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c439d0d6-def3-4e1c-a062-20413d96e4fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "279d6d50-1399-42fd-9fda-deda14ddb4d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}