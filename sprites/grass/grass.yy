{
    "id": "3ddeeb1b-4166-4138-8f27-c1e64871d7bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59a920f8-ef62-4aaf-92a9-ddd620b86523",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ddeeb1b-4166-4138-8f27-c1e64871d7bd",
            "compositeImage": {
                "id": "0107fc74-188b-4d0b-8eb8-887aa2a38d3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59a920f8-ef62-4aaf-92a9-ddd620b86523",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a814a87-e903-4e71-8500-fe833090eb3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59a920f8-ef62-4aaf-92a9-ddd620b86523",
                    "LayerId": "fb0e7afd-7f41-4f1c-8e87-80a1c019dcb1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fb0e7afd-7f41-4f1c-8e87-80a1c019dcb1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ddeeb1b-4166-4138-8f27-c1e64871d7bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}