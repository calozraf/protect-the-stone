{
    "id": "0520e449-c915-471a-8978-0ea51e52d347",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSilver",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "909cd539-59d6-41d7-ae04-5f671f461ac5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0520e449-c915-471a-8978-0ea51e52d347",
            "compositeImage": {
                "id": "a68eed96-6e23-499c-b81f-20276d570649",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "909cd539-59d6-41d7-ae04-5f671f461ac5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c250f9f-455f-4244-a476-390be5e9dc2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "909cd539-59d6-41d7-ae04-5f671f461ac5",
                    "LayerId": "93be34e6-86a9-47fe-a4be-d6bc3ef43eb3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "93be34e6-86a9-47fe-a4be-d6bc3ef43eb3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0520e449-c915-471a-8978-0ea51e52d347",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}