{
    "id": "4142f5f5-6bd1-4cac-bb6e-59ca19bf4524",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sClose",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "704b1fa7-ef9d-4072-be83-01417715825d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4142f5f5-6bd1-4cac-bb6e-59ca19bf4524",
            "compositeImage": {
                "id": "b098c3f9-cf37-46f5-a6a0-664acd09ab5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "704b1fa7-ef9d-4072-be83-01417715825d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c2fd159-fb71-40a5-a9f9-12d30c118a1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "704b1fa7-ef9d-4072-be83-01417715825d",
                    "LayerId": "21cea07c-deba-4852-a5a5-54be00fd35c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "21cea07c-deba-4852-a5a5-54be00fd35c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4142f5f5-6bd1-4cac-bb6e-59ca19bf4524",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}