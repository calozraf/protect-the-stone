{
    "id": "cfea4fc5-27b7-4992-ba1f-4b0141d83f20",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRessourceCrosser",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ae44db7-bf70-4bc6-b38a-63b187a92809",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfea4fc5-27b7-4992-ba1f-4b0141d83f20",
            "compositeImage": {
                "id": "733e261d-4237-49a8-a9b3-d4c4fb526915",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ae44db7-bf70-4bc6-b38a-63b187a92809",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e177482-0d51-415e-8bcf-581c8d863f4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ae44db7-bf70-4bc6-b38a-63b187a92809",
                    "LayerId": "4deba083-6f59-4ef1-a28f-a05c3092b1ec"
                }
            ]
        },
        {
            "id": "2c391758-e126-464f-84ce-95621300eaca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfea4fc5-27b7-4992-ba1f-4b0141d83f20",
            "compositeImage": {
                "id": "ef3538b5-e4fe-4c47-bdcf-73c4f8e6d41e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c391758-e126-464f-84ce-95621300eaca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ec0ecb2-5c3e-440e-9851-0d3d4622796f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c391758-e126-464f-84ce-95621300eaca",
                    "LayerId": "4deba083-6f59-4ef1-a28f-a05c3092b1ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4deba083-6f59-4ef1-a28f-a05c3092b1ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cfea4fc5-27b7-4992-ba1f-4b0141d83f20",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}