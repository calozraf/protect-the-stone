{
    "id": "ca6b1d93-da12-48a4-a9c3-4fc6fe67311b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGold",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "936b4030-5106-4b24-81be-84f4cab9d06b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca6b1d93-da12-48a4-a9c3-4fc6fe67311b",
            "compositeImage": {
                "id": "eb036bdf-736f-4a79-9d7f-06a942699e96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "936b4030-5106-4b24-81be-84f4cab9d06b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c729b6eb-c5e9-4cf4-845a-dbed002df64f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "936b4030-5106-4b24-81be-84f4cab9d06b",
                    "LayerId": "b81979d9-3e45-4a5d-910e-78a8ded0cd41"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b81979d9-3e45-4a5d-910e-78a8ded0cd41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca6b1d93-da12-48a4-a9c3-4fc6fe67311b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}