{
    "id": "b7aac2ff-a262-4477-b960-0e12044ec10e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEarthTemple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 2,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c7d209c-0ab7-4c65-b832-f980c94d6f10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7aac2ff-a262-4477-b960-0e12044ec10e",
            "compositeImage": {
                "id": "a8efdf8f-d495-4b7e-9039-20a9da02b618",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c7d209c-0ab7-4c65-b832-f980c94d6f10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8222aede-3d43-48ca-bcbd-1970fccb0827",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c7d209c-0ab7-4c65-b832-f980c94d6f10",
                    "LayerId": "d76828f0-2a42-4656-9500-652d35e024b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d76828f0-2a42-4656-9500-652d35e024b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7aac2ff-a262-4477-b960-0e12044ec10e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}