{
    "id": "1e7e1fea-1341-46e2-bb08-f254b9e65fd2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonOverlay",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a63becf6-db5c-413e-9403-e7696fa788d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e7e1fea-1341-46e2-bb08-f254b9e65fd2",
            "compositeImage": {
                "id": "0b9af806-7c53-46b1-a5be-a405270ebc40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a63becf6-db5c-413e-9403-e7696fa788d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd3c963a-3f03-422e-ba80-364e13c7d439",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a63becf6-db5c-413e-9403-e7696fa788d5",
                    "LayerId": "a1692660-d765-4be4-a32f-33be5cd8bdb6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a1692660-d765-4be4-a32f-33be5cd8bdb6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e7e1fea-1341-46e2-bb08-f254b9e65fd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}