{
    "id": "217e36cd-be5c-4195-99ab-384f4b21880a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOwlWasp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e3b4abd-cdbb-4adf-9af5-de7865c5a719",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "217e36cd-be5c-4195-99ab-384f4b21880a",
            "compositeImage": {
                "id": "17ec6ba0-3a55-4204-8eb7-327a113bd039",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e3b4abd-cdbb-4adf-9af5-de7865c5a719",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "500cdf12-aa22-4535-a073-2d57f49d2531",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e3b4abd-cdbb-4adf-9af5-de7865c5a719",
                    "LayerId": "e24e59f8-d0e8-43e8-b20d-598e22aa0b9d"
                }
            ]
        },
        {
            "id": "af5800ca-65bd-44f7-990d-fb0defc02d5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "217e36cd-be5c-4195-99ab-384f4b21880a",
            "compositeImage": {
                "id": "d21de347-43b7-41ac-8973-6295b810dea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af5800ca-65bd-44f7-990d-fb0defc02d5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "511ccca1-9a20-471d-aa43-e8535a5f2968",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af5800ca-65bd-44f7-990d-fb0defc02d5e",
                    "LayerId": "e24e59f8-d0e8-43e8-b20d-598e22aa0b9d"
                }
            ]
        },
        {
            "id": "a3203d57-bf22-45c0-ae66-ec997926ec27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "217e36cd-be5c-4195-99ab-384f4b21880a",
            "compositeImage": {
                "id": "5d686f7a-d217-4238-a894-9aab1a4b5293",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3203d57-bf22-45c0-ae66-ec997926ec27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee1a2015-7e27-489a-9300-333f0f160e3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3203d57-bf22-45c0-ae66-ec997926ec27",
                    "LayerId": "e24e59f8-d0e8-43e8-b20d-598e22aa0b9d"
                }
            ]
        },
        {
            "id": "c4b2b1bb-2b7a-46f5-9d9d-3b610570d045",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "217e36cd-be5c-4195-99ab-384f4b21880a",
            "compositeImage": {
                "id": "3fa04cb2-3e49-4d67-9dd4-29369b85d4cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4b2b1bb-2b7a-46f5-9d9d-3b610570d045",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b2a58ea-0ce4-4174-8797-51c0bb66d3e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4b2b1bb-2b7a-46f5-9d9d-3b610570d045",
                    "LayerId": "e24e59f8-d0e8-43e8-b20d-598e22aa0b9d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e24e59f8-d0e8-43e8-b20d-598e22aa0b9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "217e36cd-be5c-4195-99ab-384f4b21880a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}