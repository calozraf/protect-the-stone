{
    "id": "dbcf53ce-0870-4507-8111-35644ed909df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSelector",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2364db19-96ed-442e-a276-b942c5afea20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbcf53ce-0870-4507-8111-35644ed909df",
            "compositeImage": {
                "id": "7c19458a-7bbd-4f87-8d58-78c062a1eff9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2364db19-96ed-442e-a276-b942c5afea20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e79ea2cc-c5d0-473e-af21-9c55b5223073",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2364db19-96ed-442e-a276-b942c5afea20",
                    "LayerId": "236f6c0e-229c-4188-968e-b330627b21a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "236f6c0e-229c-4188-968e-b330627b21a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dbcf53ce-0870-4507-8111-35644ed909df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}