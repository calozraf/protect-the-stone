{
    "id": "c4eddfbb-c7a8-4bb0-999a-46f3d3cca217",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_consolas_small",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "07418c3a-b737-4acf-802c-86b5b40de32b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "55a0f291-364c-49e7-a4af-b945f41a8521",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 13,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 41,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "9a7ca8af-b0c0-4d7c-9f7d-77cd99d57c97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 35,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "9860d1ca-e518-42cb-bf26-2097c2c44858",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 27,
                "y": 62
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e105fd87-41fe-4058-a470-27708b554cf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 19,
                "y": 62
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ad7736f6-3316-436e-ba0b-83ac2149dbe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 11,
                "y": 62
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "d1cb81f2-bc5b-4834-a749-57a507e99e69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "217604d9-403f-4b67-bdc8-2c61fead1b91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 13,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 117,
                "y": 47
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "04100b9b-4430-4f57-ab06-fee511616230",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 111,
                "y": 47
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "70f88cab-79a7-41ce-9ed3-94fe9110d83e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 105,
                "y": 47
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "742f9eb0-bab2-449b-a3a6-476d0b19644c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 45,
                "y": 62
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "716abfc8-836b-4caf-8f65-f47bd3f7afb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 97,
                "y": 47
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ae3e0aca-ec26-4642-b055-15235d711d1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 84,
                "y": 47
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "76d410f4-66fd-4b5c-bfd7-acc9535c66ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 78,
                "y": 47
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "c8736619-37f2-42a6-b379-a7e8e1057643",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 13,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 74,
                "y": 47
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "53cb2ce6-c753-461d-9c88-5eaed8109ace",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 66,
                "y": 47
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ebe10c86-3c33-4fbb-a210-db4ec2783d9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 58,
                "y": 47
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0181ce0d-41ed-47b4-a513-c5d8b4a2feff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 47
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "311ebd23-0ef3-4473-99b2-215928767a65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 42,
                "y": 47
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "9cb6f47f-5d94-41e4-bb06-75a6a5b48129",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 47
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ce549dd8-b720-4607-b43f-4b8d377831b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 47
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c13b25a7-b82d-4cdb-abce-3849bb5a7b86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 89,
                "y": 47
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c3f0026e-d463-4a6d-9ab2-e51d9c0c6f2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 53,
                "y": 62
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ff0f6738-6eab-47f5-a6b2-c5e58aced858",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 61,
                "y": 62
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "975af026-7842-460f-be3b-a5b985b74d03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 69,
                "y": 62
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "5ebe341f-21aa-46bf-b0e1-1f48d869e172",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 101,
                "y": 77
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7d178755-4a90-4279-bb76-cf3251569d21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 13,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 97,
                "y": 77
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "1381e265-abc6-43fa-a583-a8fe3185b75a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 92,
                "y": 77
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "89bf51b2-fdb9-4303-808b-8912e633a583",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 85,
                "y": 77
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c8167937-7cb0-403d-ad47-bb3f822854bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 77,
                "y": 77
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "dc2a0f8d-6f52-480a-8351-f9d19aa26064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 70,
                "y": 77
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "b8268b25-8723-4a4a-9a7e-6fefc29925cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 64,
                "y": 77
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "67b0bbd1-b77b-4474-939c-78d003974783",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 56,
                "y": 77
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "060dcc70-ec7c-40fc-8c20-a26e445381d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 48,
                "y": 77
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "9968c5a7-35f6-4905-9349-1cb2f0550cd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 40,
                "y": 77
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "94b7e346-05c5-43e6-8732-0c7178fd548c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 32,
                "y": 77
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "1b9a9bbe-f3b9-46c4-85c0-adc637d9d7a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 24,
                "y": 77
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2816bffd-8848-42a2-9d9d-8a3586d726ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 17,
                "y": 77
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "cfbc853e-2c1c-4f7a-93d4-3836aaad9a53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 10,
                "y": 77
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "607f8e4e-0176-4223-ad68-181234671813",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 77
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f2d6ce16-2c64-49de-8447-d52a987322e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 115,
                "y": 62
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "663d64d2-9cb0-478b-9cab-ba8073ab111b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 107,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "7c9608e5-9e51-4188-8fcc-2a8ec9dc9f16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 100,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ef4c07bb-89da-4d3f-b728-2a7aa0dbff55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 92,
                "y": 62
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "57990a36-23ce-4dff-865e-bc3850ef60c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 85,
                "y": 62
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6b863810-7d82-4b26-9d1f-001baed4ef81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 77,
                "y": 62
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "8805d3e2-869f-4f85-a54b-a221e80d1b94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 47
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "23f116a6-caf1-4fee-ae46-4d981ca6e2a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 47
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "87d26b82-19ba-4488-a5a4-6bfa86f47630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 47
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "ff91927f-3201-4bed-907f-9a68cd1833d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 57,
                "y": 17
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "19a5b4ff-5393-4998-af87-bec51fef5222",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 43,
                "y": 17
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "1107936c-6b24-4205-9c8d-b04eb8cb28f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 35,
                "y": 17
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "9ea9b208-a43d-4d66-863c-8a411f8ec21b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 27,
                "y": 17
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1e78d74b-edd9-42c1-92f2-bc583fe2a58c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 19,
                "y": 17
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "cd616e5b-9d7b-47b7-a3a2-225f8dc71a09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 10,
                "y": 17
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a308d964-d056-4ac4-b570-1799db0c2a03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 17
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "456a885b-6b68-44ff-95a0-d87c04d22ce2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "fb336298-75e6-4a64-84dd-9a52c6fcd7aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "dc0ca7c1-a127-42fc-ad1d-0457b4bbb0f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "e3674a47-6f5b-4164-8cf8-fab9e0b1d8dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 51,
                "y": 17
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "bc26c5fd-dacb-471a-8d0b-8a812ef944d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "911a81f9-a6ea-49da-8300-3ecc73144d12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "cbf546a3-c147-45e3-94ea-cf5b58a80b99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "22ed9ec2-9fdb-44c9-b511-bd69578f70da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "098f5ca7-0edb-43d9-b4c5-95eb0c4538eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "86280085-ef69-479d-b1c3-0c961a7cc601",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "06eedf47-4250-45a9-8595-0f78cd0e0034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d051c39d-d7ea-413e-a77e-006b3246c090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ce94be85-8618-4804-a619-86711134f031",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "04a04555-d1ed-45d2-87b4-3275a64c0aca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "3468fa2e-1a27-4903-83cb-1fa50cd07015",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "b43fa3ca-4a01-4de1-920a-000f461dff99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 66,
                "y": 17
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "cae1276b-644a-4a1a-8df8-13244c2aa19a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 32
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "574c6349-79ff-4138-bb9d-6587b0efcd9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 17
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d80630b8-9213-4226-9277-73d050d46fe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 105,
                "y": 32
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e93ae43a-a06f-4a05-a117-65e8cf263815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 97,
                "y": 32
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f3d1d406-7138-4eac-a627-1fd3f1ba7c71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 89,
                "y": 32
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "7e869819-cef4-40e5-a9d1-0bf0cca09855",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 81,
                "y": 32
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "0feae718-8960-4d89-b766-405900e5051b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 73,
                "y": 32
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "2e7eef1e-b197-4bc5-80f0-5a9f09609201",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 65,
                "y": 32
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "bdc4a0bd-2c86-4bff-99f3-d0754e2c8c18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 57,
                "y": 32
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8e972e46-4f07-404b-97b5-a7159957bc35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 49,
                "y": 32
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8336b369-4346-46bd-be99-27d281f0bdca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 42,
                "y": 32
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d88622b0-f900-42ed-a576-10ddac741cfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 112,
                "y": 32
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "cb4908bf-cc01-4bcb-b0ca-95500a9041c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 32
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "26d64729-df73-4e7d-83bd-504c28993eeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 32
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "50068c78-502d-4601-a7dd-3d6f6793ce3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 32
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d46ecb8d-114c-4ca9-afdc-bc3cfab2c03b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c465b367-df12-4cfc-8a65-1fd698fc943a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 116,
                "y": 17
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "368f07d2-7d6b-4d8a-9cc0-9b7501fb9ffd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 108,
                "y": 17
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "052bf3dc-3814-40e1-8580-16b39616edc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 100,
                "y": 17
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "86fbf7ee-b368-42a8-8317-07dc1286d218",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 93,
                "y": 17
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "faed925b-13da-4bd5-8916-5165882eb4cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 13,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 89,
                "y": 17
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "4f9a32bb-64b7-494f-9eaf-91dcd13ce0ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 82,
                "y": 17
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "bd581b88-1ddf-4837-b15d-cb81b27f5c2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 109,
                "y": 77
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "8e9eee64-0205-4d71-854c-cc0cd7be5469",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 13,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 117,
                "y": 77
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 8,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}