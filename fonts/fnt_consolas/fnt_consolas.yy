{
    "id": "5f721d2f-7134-433e-81aa-e2d03898c5b4",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_consolas",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "3f50dec1-a310-43a7-a238-35da5cf823e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "80bc5ae1-ed2b-4c7c-ad41-d2b8aa027cb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 149,
                "y": 44
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "bb464572-79c2-40ff-9521-e23ecec5f29a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 142,
                "y": 44
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "247a7fad-31c2-4c03-8a24-ad1845ea29c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 131,
                "y": 44
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "80632bc8-c717-4326-9293-688d32e30dde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 121,
                "y": 44
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "86261dd3-b74e-40ac-a1b5-715e5da16342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 110,
                "y": 44
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "7f7480ce-cda2-4f6b-b7de-c562c21362bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 99,
                "y": 44
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f4323d3d-6f1a-45c5-bb5d-149c0d8f2b50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 94,
                "y": 44
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2b5f27ba-9971-45fc-acf7-0de47cde81a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 87,
                "y": 44
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5fa2c288-4e3d-4756-b35d-5ee9ad25de1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 80,
                "y": 44
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "d12a4536-4d27-4973-9002-f566d00fe391",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 154,
                "y": 44
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0838c08c-c920-41e8-9ab8-bf4646669945",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 69,
                "y": 44
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "0c6ac379-da04-4ed4-91f0-8cbde5670236",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 53,
                "y": 44
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "f6353a6f-2a2b-49cb-b6f3-da4633f38710",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 46,
                "y": 44
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "326838f8-fa97-4c4f-87cc-974aeb52e547",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 41,
                "y": 44
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "7332bf1e-0426-4fd5-9668-e2b39240aca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 31,
                "y": 44
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d890bc68-45d8-4dc7-a1d4-04fdf8bfe181",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 20,
                "y": 44
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "7445bffb-6981-4ef6-ba05-51b624d5f021",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 44
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "9ae909bc-ae10-4c25-b525-873d0e6238a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b0e85165-ebfb-4d60-87c7-2b5ab3f53016",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 243,
                "y": 23
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7208eb3b-919e-4d57-94d0-1e1cd53ac8db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 232,
                "y": 23
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "03f16769-3d3d-4f84-a5a6-1c89984d6160",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 60,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "115d53da-efd7-41ca-83c6-be42622261e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 163,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e8f7fe01-d3a1-48b1-9f8b-62c790996b91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 173,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8041cac2-9f51-46ff-a6c6-b9556f0c46cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 183,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d7efc688-6c8e-44e7-bb46-e2c6930088d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 129,
                "y": 65
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ed01aae6-e5a0-4ec8-b700-fa7cd8e02d0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 124,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "6375bb47-0df0-4317-88e6-5ffb64059c35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 117,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "906ff1af-65f2-4103-8094-ea9efaff01fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 108,
                "y": 65
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5d2ab6a9-1213-4c87-8cd7-610b156a181c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 99,
                "y": 65
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "59616ba0-cd16-44a8-b342-f1163003ec5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 90,
                "y": 65
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d3e8718b-9a04-4bf5-a99e-b1be24682f09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 82,
                "y": 65
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d88ccf8b-8d1c-48d5-9030-f673afe72310",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 71,
                "y": 65
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "82580004-c61c-4e78-b52f-8c4b0a31aa68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 60,
                "y": 65
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e6ca0df5-8d39-4548-bb61-394f9bb623ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 51,
                "y": 65
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "48f9de42-ab2d-4876-891e-4cfd2c67faa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 41,
                "y": 65
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "bcd3361e-7d85-4240-893a-c9af6fb473d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 30,
                "y": 65
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c8b1453d-60cb-44b4-866f-e4f98667e085",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 21,
                "y": 65
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "26a9c2af-fd7e-439c-a728-9485df01e05e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 12,
                "y": 65
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e316208d-3f12-4905-a161-7d7828ee9775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "41230b57-1350-4362-8e2a-4e735242cfd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 240,
                "y": 44
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "68c00562-83b8-4f38-b8d3-b1ec6d09f3b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 231,
                "y": 44
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "772e8236-d722-44af-8ac0-61c08e7dba85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 223,
                "y": 44
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "d032896e-819e-4d06-8e74-517b8e530931",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 213,
                "y": 44
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d5fefca9-53df-4a77-9a66-c6a47d27f7df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 204,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "cfb17261-5d37-4cf7-a86c-dcd70a9543e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 193,
                "y": 44
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "3da96099-1558-4221-afb4-952e15d84c74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 222,
                "y": 23
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "03cc058e-1d52-4998-bb1f-1fd93f2bbca5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 211,
                "y": 23
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "8114a430-8426-4b1f-b145-ff714416da07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 202,
                "y": 23
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e4b08ccb-6cbd-426e-8c01-96f0bd285d87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "80adbb62-356c-44b9-84d3-8ed7572e6eee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "cba2e398-d925-44f8-a7cf-5f8f79d6a2d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "cd133eff-f1e0-408c-921c-1dd11936acb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c2f044af-7765-4f7e-90a0-53f2bb41d233",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "6ee57115-c219-4f64-8409-bc693787f6cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "12277d26-a072-4a43-b495-f8c3e00c7fd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ac27b1bd-a6ee-4157-806f-36b8bd925078",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "2622fafb-0e4e-4874-bb5b-c013aa4319fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4068d57f-4667-4532-adf4-d8e74a0b3f14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "bc82d051-4e3f-4996-88f8-96a4aa7c3839",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "daf31638-93f2-4a44-89b8-b77b96a2241c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e875a8e7-e54a-4d7a-bce0-977665a5855b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "836ba413-1c46-4233-ba71-947058d533bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "7bc36640-262a-4131-bb05-b5cb5642c3e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "b0b7b360-e02d-4077-b3c1-220d6ca2d6de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 6,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "ac7cd3e0-d5d9-4143-ad0c-8c713d14a72f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b7495286-605e-4217-be94-d78dc345cc41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ba6f9cfb-9ae7-4b28-9bb5-269d1b4bdb47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d22e7541-5d1a-4bef-8a4a-f62f1ca4b7dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "285d8e82-4fa5-445d-8525-44e30567edcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "3d3488af-1fc0-45d2-9e24-65485e845e7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "1998bd65-c46a-406c-a966-383f9ae7e298",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "9f34b05d-7ac5-405f-be43-b37ff5beb0a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 87,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "026f0e21-d6ef-4fa5-8014-9b9c58dc4bac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "758fc9b0-0b6d-45d4-8568-2d5555f2d392",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 185,
                "y": 23
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b485a30f-e7ef-482b-b705-19612e0dde2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 175,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2ddc4044-bbbc-47f6-b0cc-69f84becfb51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 166,
                "y": 23
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "217c8b12-ba8e-42f9-8e9f-4ed1abf8bb84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 155,
                "y": 23
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "39d5fc79-b5c4-4457-bbd4-51034d9fe701",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 146,
                "y": 23
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "eef3eea6-cdb3-4ecc-8f22-5e64a8bec4e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 135,
                "y": 23
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "34926049-b53a-4e51-ba7b-9e2ba5271322",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 126,
                "y": 23
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "47d18c72-3645-458a-8d42-7b6cc7d347c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 116,
                "y": 23
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b6d6f964-bc66-49e6-89b5-d6398a1677f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 106,
                "y": 23
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "80d16afb-db67-4698-866e-043d5adcf02a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 193,
                "y": 23
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "0483ae27-50c6-432c-b016-4c5e805b9557",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 96,
                "y": 23
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "49d43d43-a36a-4957-8b7b-9ebc07105d5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 78,
                "y": 23
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "bfd72e96-ab5a-4764-b976-9c0cff3d097f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 67,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "3420e9e7-ff31-46b6-8701-5af79273d892",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 56,
                "y": 23
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f11080b3-9a80-4aef-881c-723f0e8d8fdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 45,
                "y": 23
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5865767f-ff2a-4a7e-a07a-a2f2f3d327b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 34,
                "y": 23
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "6242ac8c-d412-480a-8a00-b715fd279d7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 25,
                "y": 23
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "386e290e-3a92-49d9-b445-eab900e175bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 16,
                "y": 23
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "4bffcb84-cb01-4b5c-b414-56b9afd4ea04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 11,
                "y": 23
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "512b6ef4-b04e-4d68-9308-dcfd01a788cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c5d52c72-9f8f-4b8c-9c59-4371186cbd38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 139,
                "y": 65
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "22fc8cb3-de3b-4a94-b5e0-fb2fd085da9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 19,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 150,
                "y": 65
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}